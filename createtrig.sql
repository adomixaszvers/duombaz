CREATE FUNCTION arapylygeskomandos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
if (select abs(((select skaicius from adja0844.komandos_nariu_skaicius where komandos_nariu_skaicius.pavadinimas=new.pirmoji_komanda)-(select skaicius from adja0844.komandos_nariu_skaicius where komandos_nariu_skaicius.pavadinimas=new.antroji_komanda)))) > 3
then raise exception 'Komandu nariu skirtumas negali buti didesnis nei 3.';
end if;
return new;
end;
$$;

CREATE FUNCTION arbuvomacas() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
if (select count(macas.pirmoji_komanda) from adja0844.macas where new.pirmoji_komanda=macas.antroji_komanda and new.antroji_komanda=macas.pirmoji_komanda and new.data=macas.data) > 0
then raise exception 'Toks macas jau buvo.';
end if;
return new;
end;
$$;

CREATE FUNCTION arperdaugzaidejukomandoje() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
if (select skaicius from adja0844.komandos_nariu_skaicius where komandos_nariu_skaicius.pavadinimas = new.komanda) > 20
then raise exception 'Komandu nariu skaicius negali buti didesnis nei 20.';
end if;
return new;
end;
$$;


CREATE FUNCTION naujaszaidejoid() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   BEGIN
     NEW.numeris = (SELECT COALESCE(MAX(numeris),0)+1 FROM adja0844.Zaidejas);
     RETURN NEW;
   END;
  $$;

CREATE TRIGGER arapyligeskomandos BEFORE INSERT OR UPDATE ON macas FOR EACH ROW EXECUTE PROCEDURE arapylygeskomandos();

CREATE TRIGGER arbuvomacas BEFORE INSERT OR UPDATE ON macas FOR EACH ROW EXECUTE PROCEDURE arbuvomacas();

CREATE TRIGGER arperdaugzaidejukomandoje BEFORE INSERT OR UPDATE ON zaidejas FOR EACH ROW EXECUTE PROCEDURE arperdaugzaidejukomandoje();

CREATE TRIGGER naujozaidejoid BEFORE INSERT ON zaidejas FOR EACH ROW EXECUTE PROCEDURE naujaszaidejoid();
