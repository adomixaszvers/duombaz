CREATE VIEW adja0844.komanda_baige_lygiosiomis AS
 WITH temp_table AS (
         SELECT komanda_1.pavadinimas, 
            count(*) AS kiek
           FROM adja0844.komanda komanda_1, 
            macas
          WHERE ((((komanda_1.pavadinimas) = (macas.antroji_komanda)) OR ((komanda_1.pavadinimas) = (macas.pirmoji_komanda))) AND (macas.pirmosios_taskai = macas.antrosios_taskai))
          GROUP BY komanda_1.pavadinimas
        )
 SELECT komanda.pavadinimas, 
    COALESCE(temp_table.kiek, (0)) AS kiek
   FROM (adja0844.komanda
   LEFT JOIN temp_table ON (((komanda.pavadinimas) = (temp_table.pavadinimas))));

CREATE VIEW adja0844.komanda_laimejo AS
 WITH temp_table AS (
         SELECT komanda_1.pavadinimas, 
            count(*) AS kiek
           FROM adja0844.komanda komanda_1, 
            macas
          WHERE ((((komanda_1.pavadinimas) = (macas.antroji_komanda)) AND (macas.pirmosios_taskai < macas.antrosios_taskai)) OR (((komanda_1.pavadinimas) = (macas.pirmoji_komanda)) AND (macas.pirmosios_taskai > macas.antrosios_taskai)))
          GROUP BY komanda_1.pavadinimas
        )
 SELECT komanda.pavadinimas, 
    COALESCE(temp_table.kiek, (0)) AS kiek
   FROM (adja0844.komanda
   LEFT JOIN temp_table ON (((komanda.pavadinimas) = (temp_table.pavadinimas))));

CREATE VIEW adja0844.komanda_pralaimejo AS
 WITH temp_table AS (
         SELECT komanda_1.pavadinimas, 
            count(*) AS kiek
           FROM adja0844.komanda komanda_1, 
            macas
          WHERE ((((komanda_1.pavadinimas) = (macas.antroji_komanda)) AND (macas.pirmosios_taskai > macas.antrosios_taskai)) OR (((komanda_1.pavadinimas) = (macas.pirmoji_komanda)) AND (macas.pirmosios_taskai < macas.antrosios_taskai)))
          GROUP BY komanda_1.pavadinimas
        )
 SELECT komanda.pavadinimas, 
    COALESCE(temp_table.kiek, (0)) AS kiek
   FROM (adja0844.komanda
   LEFT JOIN temp_table ON (((komanda.pavadinimas) = (temp_table.pavadinimas))));


CREATE VIEW adja0844.komandos_nariu_skaicius AS
 WITH temp_table AS (
         SELECT komanda_1.pavadinimas, 
            count(zaidejas.vardas) AS skaicius
           FROM adja0844.komanda komanda_1, 
            zaidejas
          WHERE ((zaidejas.komanda) = (komanda_1.pavadinimas))
          GROUP BY komanda_1.pavadinimas
        )
 SELECT komanda.pavadinimas, 
    COALESCE(temp_table.skaicius, (0)) AS skaicius
   FROM (adja0844.komanda
   LEFT JOIN temp_table ON (((komanda.pavadinimas) = (temp_table.pavadinimas))));

