DROP TRIGGER arapylygeskomandos ON adja0844.macas;
DROP TRIGGER arbuvomacas ON adja0844.macas;
DROP TRIGGER arperdaugzaidejukomandoje ON adja0844.zaidejas;
DROP TRIGGER naujaszaidejoid ON adja0844;

DROP FUNCTION adja0844.arapylygeskomandos();
DROP FUNCTION adja0844.arbuvomacas();
DROP FUNCTION adja0844.arperdaugzaidejukomandoje();
DROP FUNCTION adja0844.naujaszaidejoid();
