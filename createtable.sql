CREATE TABLE adja0844.adresas (
    pasto_kodas integer NOT NULL,
    pastato_numeris integer,
    gatve varchar(30)  DEFAULT 'Nežinoma',
    gyvenviete varchar(30) NOT NULL,
    rajonas varchar(30) NOT NULL
);

CREATE TABLE adja0844.komanda (
    pavadinimas varchar(30) NOT NULL,
    CONSTRAINT komanda_pavadinimas_check CHECK (((pavadinimas) <> ''))
);

CREATE TABLE adja0844.macas (
    pirmoji_komanda varchar(30) NOT NULL,
    antroji_komanda varchar(30) NOT NULL,
    pirmosios_taskai integer DEFAULT 0,
    antrosios_taskai integer DEFAULT 0,
    data date NOT NULL,
    CONSTRAINT macas_check CHECK (((pirmoji_komanda) <> (antroji_komanda)))
);

CREATE TABLE adja0844.zaidejas (
    vardas varchar(30) NOT NULL,
    pasto_kodas integer NOT NULL,
    komanda varchar(30) NOT NULL,
    numeris integer,
    CONSTRAINT zaidejas_vardas_check CHECK (((vardas) <> ''))
);

ALTER TABLE ONLY adja0844.adresas
    ADD CONSTRAINT adresas_pkey PRIMARY KEY (pasto_kodas);

ALTER TABLE ONLY adja0844.komanda
    ADD CONSTRAINT komanda_pkey PRIMARY KEY (pavadinimas);


ALTER TABLE ONLY adja0844.macas
    ADD CONSTRAINT macas_pkey PRIMARY KEY (pirmoji_komanda, antroji_komanda, data);


ALTER TABLE ONLY adja0844.zaidejas
    ADD CONSTRAINT zaidejas_pkey PRIMARY KEY (vardas);

ALTER TABLE ONLY adja0844.macas
    ADD CONSTRAINT macas_antroji_komanda_fkey FOREIGN KEY (antroji_komanda) REFERENCES adja0844.komanda(pavadinimas) ON DELETE RESTRICT;

ALTER TABLE ONLY adja0844.macas
    ADD CONSTRAINT macas_pirmoji_komanda_fkey FOREIGN KEY (pirmoji_komanda) REFERENCES adja0844.komanda(pavadinimas) ON DELETE RESTRICT;

ALTER TABLE ONLY adja0844.zaidejas
    ADD CONSTRAINT zaidejas_komanda_fkey FOREIGN KEY (komanda) REFERENCES adja0844.komanda(pavadinimas);

ALTER TABLE ONLY adja0844.zaidejas
    ADD CONSTRAINT zaidejas_pasto_kodas_fkey FOREIGN KEY (pasto_kodas) REFERENCES adja0844.adresas(pasto_kodas);

